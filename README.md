# Présentation
Cet outil permet d'obtenir une vue alignée d'une traduction ou "texte à aligner", par rapport à l'édition que nous prenons comme référence : celle de Coulon.

Il ne fait que proposer une visualisation des alignements. C'est à vous jouer en "réparant" les défauts d'alignement, dus à d'éventuelles erreurs d'encodage, choix de traduction, etc.

Pour cela, il faut observer la visualisation et, pour les cas de « correspondance à corriger » (mis en valeur en jaune), déclarer - dans le texte à aligner - le numéro de la prise de parole (ou réplique) correspondante au sein de l'édition de référence.
Par exemple, dans l'exemple suivant, on déclare que la réplique de Carion correspond à la 37e réplique de l'édition de référence grâce à l'attribut `@corresp` de l'élément `<sp/>` qui prend la valeur "37":
```xml
<sp corresp="37">
  <speaker corresp="ΧΡΕΜΥΛΟΣ">CARION</speaker>
  <note place="inline" resp="#MLJ"> Traduction dans le sommaire, vers 80 dans le grec</note>
  <p>Toi, Plutus, bâti comme te voilà!</p>
</sp>
```


**Une démonstration de la sortie est disponible sur https://litt-arts-num.gitlab.io/taf-parallel-view/**


# Pré-requis :
Cet outil fonctionne sur des fichiers XML-TEI encodés selon le modèle d'encodage du projet TAF.

Le texte à aligner :
* doit contenir, en ligne 2, l'instruction suivante :
```xml
<?xml-stylesheet type="text/xsl" href="TAF_Alignement.xsl"?>
```
* doit contenir la déclaration du texte de référence de Coulon encodée ainsi :
```xml
<TEI>
  <teiHeader>
    (...)
    <fileDesc>
      (...)
      <sourceDesc>
        (...)
        <listBibl type="reference_edition">   <!-- l'attribut @type avec la valeur "refenrece_edition" est nécessaire -->
          <bibl xml:id="plutus_coulon">       <!-- indiquer de préférence un @xml:id ici -->
            <author xml:id="coulon"/>         <!-- indiquer ici l'identifiant de l'auteur de l'édition de référence -->
            <ref target="Plutus-Coulon.xml"/> <!-- indiquer ici le chemin vers le fichier contenant l'édition de référence -->
          </bibl>
        </listBibl>
      </sourceDesc>
      (...)
</TEI>
```

# Comment utiliser cet outil d'alignement ?
## Sous Oxygen
* ouvrir Oxygen
* ouvrir le texte à aligner (dans ce cas, PLUTUS_Brumoy_MJL.xml)
* appliquer le scénario de transformation en utilisant l'une des méthodes suivantes (au choix) :
  * aller dans le menu Document > Transformation > Appliquer le/les scénario(s) de transformation
  * utiliser le raccourcis clavier CTL+MAJ+T (sous Mac, ???)
  * cliquer sur l'icône représentant un triangle rouge inscrit dans un cercle blanc
* déclarer une correspondance (voir section "Créer des points d'alignement") et relancer la transformation
* renouveler l'opération autantde fois que nécessaire !

À savoir :
* la 1ère fois qu'on lance la transformation, répondre "Oui" lorsque la fenêtre affiche « Éxectuer le scénario par défaut sur la base de l'instruction de traitement "xml-stylesheet" ? ». Les autres fois, cela se fera de façon automatique.
* la transformation devrait (mais cela peut éventuellement changer selon votre système d'exploitation, Mac, Windows, etc.) produire deux choses :
  * la création (ou la mise à jour) d'un fichier de sortie HTML
  * l'ouverture dans votre navigateur Internet de celui-ci

## En ligne de commande
Installer si nécessaire `sudo apt install libsaxonb-java` et executer la commande `saxonb-xslt -xi PLUTUS_Brumoy_MJL.xml TAF_Alignement.xsl > PLUTUS_Brumoy_MJL.html`

# Exemple de sortie produite avec alignement à corriger
<img src="img/TAF_Alignement_a_corriger.png" width="80%" style="max-width: 500px;">

# Créer des points d'alignement
Conseil : avant de lancer l'outil pour la 1e fois, il est conseillé de déclarer les alignements connus sur les noms des personnages. Cela peut se faire sur l'ensemble de la source TEI grâce à des opérations rechercher/remplacer. L'encodage d'une correspondance entre personnages se fait grâce à l'attribut `@corresp` sur l'élément `<speaker/>` :
```xml
<sp>
  <speaker corresp="ΧΡΕΜΥΛΟΣ">CHREMYLE</speaker>
  <p>(...)</p>
</sp>
```
Pour créer de nouveau point d'alignement :
  * dans le texte à aligner, définir au sein de la prise de parole en question (`<sp>`) l'attribut `@corresp` (sous la forme `@corresp="NUM"` avec NUM le numéro du `<sp>` du corpus de référence
  * **relancer la transformation** et répéter les étapes autant que nécessaire.
    
**Important :** commencer de préférence par le début du texte. L'alignement se fait ensuite en cascade et il est fort probable qu'en définissant un nouveau point d'alignement, la suite du texte s'aligne de lui-même correctement.
