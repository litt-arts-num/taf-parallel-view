<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:xr="http://www.oxygenxml.com/ns/xmlRefactoring" exclude-result-prefixes="xs" version="2.0">
    <xsl:variable name="stylesheet_version">4.1 - 17/05/2021</xsl:variable>

    <xsl:output method="html" indent="yes" doctype-public="html" encoding="UTF-8"/>
    <xsl:variable name="DEBUG" as="xs:boolean">1</xsl:variable>
    <xsl:variable name="reference_author">coulon</xsl:variable>
    <xsl:variable name="current_edition" as="node()" select="/tei:TEI"/>
    <xsl:template match="/">
        <html lang="fr gr">
            <head>
                <!-- Required meta tags -->
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>

                <!-- Bootstrap CSS -->
                <link
                    href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
                    crossorigin="anonymous"/>

                <!-- Option 1: Bootstrap Bundle with Popper -->
                <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"/>

                <script src="https://use.fontawesome.com/8ec538547e.js"/>
                <script src="https://code.jquery.com/jquery-3.5.1.min.js"/>
                <script>
                    function next_tr_warning(){
                      var first = $("#first");
                      if (first.nextAll(".tr-warning").length > 0){
                        first.removeClass("highlight");
                        first.removeAttr("id");
                        first.nextAll(".tr-warning").first().attr('id', 'first');
                        $('html, body').animate({
                          scrollTop: $("#first").offset().top
                        }, 250);
                      } else {
                        alert("Il n'y a pas d'autre correspondance à corriger");
                      }
                    }
                    function previous_tr_warning(){
                      var first = $("#first");
                      if (first.prevAll(".tr-warning").length > 0){
                        first.removeClass("highlight");
                        first.removeAttr("id");
                        first.prevAll(".tr-warning").first().attr('id', 'first');
                        $('html, body').animate({
                          scrollTop: $("#first").offset().top
                        }, 250);
                      } else {
                        alert("Il n'y a pas de précédente correspondance à corriger");
                      }
                    }
                    function next_tr_warning_group(){
                      var first = $("#first");
                      if (first.nextAll(".tr-corresp").nextAll(".tr-warning").length > 0){
                        first.removeClass("highlight");
                        first.removeAttr("id");
                        first.nextAll(".tr-corresp").nextAll(".tr-warning").first().attr('id', 'first');
                        $('html, body').animate({
                          scrollTop: $("#first").offset().top
                        }, 250);
                      } else {
                        alert("Il n'y a pas d'autre groupe de correspondance à corriger");
                      }
                    }
                    function previous_tr_warning_group(){
                      var first = $("#first");
                      if (first.prevAll(".tr-corresp").prevAll(".tr-warning").first().prevAll(".tr-corresp").first().next().length > 0){
                        first.removeClass("highlight");
                        first.removeAttr("id");
                        first.prevAll(".tr-corresp").prevAll(".tr-warning").first().prevAll(".tr-corresp").first().next().attr('id', 'first');
                        $('html, body').animate({
                          scrollTop: $("#first").offset().top
                        }, 250);
                      } else {
                        alert("Il n'y a pas de précédent groupe de correspondance à corriger");
                      }
                    }
                </script>
                <style>
                    .highlight {
                        background-color: red;
                    }
                    .correspOne,
                    .correspTwo {
                        font-family: Arial, serif;
                        font-size: small;
                        tex-align: right;
                        color: #999;
                    }
                    .one,
                    .two {
                        /*width: 45%;
                        padding: 5px 15px;*/
                        text-align: justify;
                        vertical-align: top;
                    }
                    .aligned {
                        /*background-color: #eee;*/
                        border: none;
                    }
                    p {
                        color: #252525;
                    }
                    .speaker {
                        text-align: center;
                        display: block;
                        color: black;
                    }
                    .speaker:hover,
                    i.fa:hover {
                        cursor: help;
                    }
                    .verse-number {
                        font-family: Arial, serif;
                        font-size: x-small;
                        float: left;
                        color: #999;
                        margin-left: -20px;
                        cursor: help;
                    }</style>
                <style type="text/css">
                    .navigation-btn {
                        cursor: pointer;
                        position: fixed;
                        right: 20px;
                        height: 32px;
                        width: 32px;
                        text-align: center;
                        vertical-align: middle;
                    }
                    #back-to-top {
                        bottom: 30px;
                    }
                    #previous-warning-group {
                        top: 30px;
                    }
                    #previous-warning {
                        top: 75px;
                    }
                    #next-warning {
                        top: 120px;
                    }
                    #next-warning-group {
                        top: 165px;
                    }</style>
            </head>
            <body class="main-container">
                <div class="text-end text-secondary small p-1">
                    <i>XSL version <xsl:value-of select="$stylesheet_version"/></i>
                </div>
                <!-- TODO : update ref edition and aligned edition ; need to use @xml:id for more precision -->
                <xsl:variable name="reference_edition"
                    select="document($current_edition/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listBibl[@type = 'reference_edition']/tei:bibl/tei:author[@xml:id = $reference_author]/../tei:ref/@target)"/>

                <xsl:choose>
                    <xsl:when
                        test="
                            not($current_edition/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listBibl[@type = 'reference_edition']/tei:bibl/tei:author[@xml:id = $reference_author]/../tei:ref/@target)
                            or not($reference_edition)">
                        <div class="container row col-12 btn btn-danger m-4 p-4">
                            <i class="fa fa-exclamation-triangle"/> Problème d'accès ou de
                            définition du texte de référence.<br/>
                            <xsl:choose>
                                <xsl:when
                                    test="not($current_edition/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listBibl[@type = 'reference_edition'])"
                                    > L'élément
                                    <pre style="display: inline">&lt;tei:listBibl @type='reference_edition'></pre>
                                    n'a pas été défini. </xsl:when>
                                <xsl:when
                                    test="
                                        $current_edition/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listBibl[@type = 'reference_edition']/tei:bibl/tei:author
                                        and not($current_edition/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listBibl[@type = 'reference_edition']/tei:bibl/tei:author[@xml:id = $reference_author])"
                                    > Aucun texte de référence de l'auteur « <b><xsl:value-of
                                            select="$reference_author"/></b> » n'a été
                                    référencé.<br/>
                                </xsl:when>
                                <xsl:when test="not($reference_edition)"> Le fichier de référence
                                    <pre style="display: inline;"><xsl:value-of select="$current_edition/tei:teiHeader/tei:fileDesc/tei:sourceDesc/tei:listBibl[@type = 'reference_edition']/tei:bibl/tei:author[@xml:id = $reference_author]/../tei:ref/@target"/></pre>
                                    n'est pas accessible.<br/>
                                </xsl:when>
                                <xsl:otherwise> L'encodage attendu au sein du
                                    <pre style="display: inline">&lt;teiHeader/></pre> doit être de
                                    la forme suivante : <div style=" margin: auto;"
                                        ><pre style="text-align: left;">
                                &lt;sourceDesc>
                                    &lt;bibl>
                                        &lt;!-- référence bibliographique de l'édition à aligner -->
                                    &lt;/bibl>
                                    &lt;listBibl type="reference_edition"> &lt;!-- l'attribut @type avec la valeur "refenrece_edition" est nécessaire -->
                                        &lt;bibl xml:id="plutus_coulon"> &lt;!-- indiquer de préférence un @xml:id ici -->
                                            &lt;author xml:id="coulon"/> &lt;!-- indiquer ici l'identifiant de l'auteur de l'édition de référence -->
                                            &lt;ref target="Plutus-Coulon.xml"/> &lt;!-- indiquer ici le chemin vers le fichier contenant l'édition de référence -->
                                        &lt;/bibl>
                                    &lt;/listBibl>
                                &lt;/sourceDesc>
                            </pre></div>
                                </xsl:otherwise>
                            </xsl:choose>
                        </div>
                    </xsl:when>
                </xsl:choose>

                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="table-responsive">
                                <table class="table table-hover table-sm table-borderless flmx-auto">
                                    <thead class="table-light">
                                        <tr>
                                            <th scope="col" class="col-1"/>
                                            <th scope="col" class="col-5 text-center align-middle">
                                                <xsl:value-of
                                                  select="$reference_edition/tei:TEI/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"
                                                />
                                            </th>
                                            <th scope="col" class="col-1"/>
                                            <th scope="col" class="col-5 text-center align-middle">
                                                <xsl:value-of
                                                  select="$current_edition/tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"
                                                />
                                            </th>
                                        </tr>
                                        <tr>
                                            <td/>
                                            <td>
                                                <p class="fs-10 fw-lighter fst-italic">
                                                  <small>
                                                  <xsl:apply-templates
                                                  select="$reference_edition/tei:TEI/tei:teiHeader/tei:fileDesc/tei:sourceDesc"
                                                  mode="do"/>
                                                  </small>
                                                </p>
                                            </td>
                                            <td/>
                                            <td>
                                                <p class="fs-10 fw-lighter fst-italic">
                                                  <small>
                                                  <xsl:apply-templates
                                                  select="$current_edition/tei:teiHeader/tei:fileDesc/tei:sourceDesc"
                                                  mode="do"/>
                                                  </small>
                                                </p>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="first">
                                            <td/>
                                        </tr>
                                        <xsl:for-each
                                            select="$current_edition//tei:sp | $current_edition//tei:div[@type = 'sp_like']/tei:p">
                                            <xsl:if
                                                test="count(./preceding-sibling::tei:sp) = 0 and count(./preceding-sibling::tei:head) >= 1">
                                                <tr>
                                                  <td/>
                                                  <td/>
                                                  <td/>
                                                  <td class="text-center">
                                                  <xsl:apply-templates
                                                  select="./preceding-sibling::tei:head"/>
                                                  </td>
                                                </tr>
                                            </xsl:if>
                                            <xsl:variable name="correspTwo"
                                                select="count(. | preceding::tei:sp)"/>
                                            <xsl:if test="$DEBUG">
                                                <xsl:message>##### <xsl:value-of
                                                  select="$correspTwo"/> #####</xsl:message>
                                            </xsl:if>
                                            <xsl:if test="$DEBUG">
                                                <xsl:message> getCorrespOne(<xsl:value-of
                                                  select="$correspTwo"/>)</xsl:message>
                                            </xsl:if>
                                            <xsl:variable name="correspOne">
                                                <xsl:call-template name="getCorrespOne">
                                                  <xsl:with-param name="correspTwo"
                                                  select="$correspTwo"/>
                                                </xsl:call-template>
                                            </xsl:variable>
                                            <xsl:if test="$DEBUG">
                                                <xsl:message> corresp L/R found: <xsl:value-of
                                                  select="$correspOne"/> / <xsl:value-of
                                                  select="$correspTwo"/></xsl:message>
                                            </xsl:if>
                                            <xsl:choose>
                                                <xsl:when test="$correspOne = 'none'">
                                                  <tr class="tr-corresp">
                                                  <td class="correspOne">-</td>
                                                  <td class="one">-</td>
                                                  <td class="correspTwo">
                                                  <xsl:value-of select="$correspTwo"/>
                                                  </td>
                                                  <td class="two aligned">
                                                  <xsl:apply-templates
                                                  select="$current_edition//tei:sp[count(. | preceding::tei:sp) = $correspTwo]"
                                                  />
                                                  </td>
                                                  </tr>
                                                </xsl:when>
                                                <!--<xsl:when test="not(empty($current_edition//tei:sp[count(. | preceding::tei:sp) = $correspTwo]/@corresp))">
                                                    <tr class="tr-corresp">
                                                        <td class="correspOne">
                                                            <xsl:value-of select="$correspOne"/>
                                                        </td>
                                                        <td class="one">
                                                            <xsl:apply-templates
                                                                select="$reference_edition/tei:TEI//tei:sp[count(. | preceding::tei:sp) = $correspOne]"
                                                            />
                                                        </td>
                                                        <td class="correspTwo">
                                                            <xsl:value-of select="$correspTwo"/>
                                                        </td>
                                                        <td class="two aligned">
                                                            <xsl:apply-templates
                                                                select="$current_edition//tei:sp[count(. | preceding::tei:sp) = $correspTwo]"
                                                            />
                                                        </td>
                                                    </tr>
                                                </xsl:when>-->
                                                <xsl:otherwise>
                                                  <xsl:variable name="previousCorrespOne">
                                                  <xsl:if test="$DEBUG">
                                                  <xsl:message> getPreviousCorrespOne(<xsl:value-of
                                                  select="$correspTwo"/>)</xsl:message>
                                                  </xsl:if>
                                                  <xsl:call-template name="getPreviousCorrespOne">
                                                  <xsl:with-param name="correspTwo"
                                                  select="xs:integer($correspTwo)"/>
                                                  </xsl:call-template>
                                                  </xsl:variable>
                                                  <xsl:if test="$DEBUG">
                                                  <xsl:message> R <xsl:value-of select="$correspTwo"/>
                                                  <xsl:text> : L </xsl:text>
                                                  <xsl:value-of select="$correspOne"/>
                                                  <xsl:text>/ previousL </xsl:text>
                                                  <xsl:value-of select="$previousCorrespOne"/>
                                                  </xsl:message>
                                                  </xsl:if>
                                                  <xsl:if
                                                      test="(xs:integer(tokenize($correspOne, ' ')[1]) - xs:integer(tokenize($previousCorrespOne, ' ')[1])) > 1">
                                                  <xsl:if test="$DEBUG">
                                                  <xsl:message>
                                                  <xsl:text>  Filling left column (L / R):</xsl:text>
                                                  </xsl:message>
                                                  </xsl:if>
                                                  <xsl:for-each
                                                      select="$reference_edition/tei:TEI//tei:sp[count(. | preceding::tei:sp) > xs:integer(tokenize($previousCorrespOne, ' ')[1]) and xs:integer(tokenize($correspOne, ' ')[1]) > count(. | preceding::tei:sp)]">
                                                  <xsl:if test="$DEBUG">
                                                  <xsl:message>
                                                  <xsl:text>    </xsl:text>
                                                  <xsl:value-of
                                                  select="count(. | preceding::tei:sp)"/>
                                                  <xsl:text> /  -  </xsl:text>
                                                  </xsl:message>
                                                  </xsl:if>
                                                  <tr class="tr-corresp">
                                                  <td class="correspOne">
                                                  <xsl:value-of
                                                  select="count(. | preceding::tei:sp)"/>
                                                  </td>
                                                  <td class="one">
                                                  <xsl:apply-templates select="."/>
                                                  </td>
                                                  <td class="correspTwo">-</td>
                                                  <td class="two aligned">-</td>
                                                  </tr>
                                                  </xsl:for-each>
                                                  </xsl:if>

                                                  <xsl:variable name="spOne">
                                                  <xsl:value-of
                                                      select="normalize-space($reference_edition/tei:TEI//tei:sp[count(. | preceding::tei:sp) = xs:integer(tokenize($correspOne, ' ')[1])]/tei:speaker/@corresp)"
                                                  />
                                                  </xsl:variable>
                                                  <xsl:variable name="spTwo">
                                                  <xsl:value-of
                                                  select="normalize-space($current_edition//tei:sp[count(. | preceding::tei:sp) = $correspTwo]/tei:speaker/@corresp)"
                                                  />
                                                  </xsl:variable>
                                                  <xsl:if test="$DEBUG">
                                                  <xsl:message>
                                                  <xsl:text>  Creating line (L / R): </xsl:text>
                                                  <xsl:value-of select="$correspOne"/>
                                                  <xsl:text>  / </xsl:text>
                                                  <xsl:value-of select="$correspTwo"/>
                                                  </xsl:message>
                                                  </xsl:if>
                                                  <tr>
                                                  <xsl:attribute name="class">
                                                  <xsl:choose>
                                                  <xsl:when test="$spOne = $spTwo">tr-corresp
                                                  aligned</xsl:when>
                                                  <xsl:otherwise>tr-warning <xsl:value-of
                                                  select="$spOne"/>-<xsl:value-of select="$spTwo"
                                                  /></xsl:otherwise>
                                                  </xsl:choose>
                                                  </xsl:attribute>
                                                  <td class="correspOne">
                                                  <xsl:choose>
                                                  <xsl:when
                                                      test="not((xs:integer(tokenize($correspOne, ' ')[1]) - xs:integer(tokenize($previousCorrespOne, ' ')[1])) = 0)">
                                                  <xsl:value-of select="$correspOne"/>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:value-of select="$correspOne"/>*
                                                  </xsl:otherwise>
                                                  </xsl:choose>
                                                  </td>
                                                  <td class="one">
                                                  <xsl:choose>
                                                  <xsl:when
                                                      test="not((xs:integer(tokenize($correspOne, ' ')[1]) - xs:integer(tokenize($previousCorrespOne, ' ')[1])) = 0)">
                                                  <xsl:apply-templates
                                                      select="$reference_edition/tei:TEI//tei:sp[count(. | preceding::tei:sp) = xs:integer(tokenize($correspOne, ' ')[1])]"
                                                  />
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <i class="fa fa-arrow-up"
                                                  title="Correspondance avec la réplique précédante"
                                                  />
                                                  </xsl:otherwise>
                                                  </xsl:choose>
                                                  </td>
                                                  <td class="correspTwo">
                                                  <xsl:value-of select="$correspTwo"/>
                                                  </td>
                                                  <td class="two">
                                                  <xsl:attribute name="class">
                                                  <xsl:text>two </xsl:text>
                                                  <xsl:choose>
                                                  <xsl:when test="$spOne = $spTwo"
                                                  >aligned</xsl:when>
                                                  <xsl:otherwise>table-warning <xsl:value-of
                                                  select="$spOne"/>-<xsl:value-of select="$spTwo"
                                                  /></xsl:otherwise>
                                                  </xsl:choose>
                                                  </xsl:attribute>
                                                  <xsl:if test="not($spOne = $spTwo)">
                                                  <span>
                                                  <xsl:attribute name="onClick">
                                                  <xsl:text>alert('Pour déclarer un alignement, chercher le chemin XPath \n     //sp[</xsl:text>
                                                  <xsl:value-of select="$correspTwo"/>
                                                  <xsl:text>]/*\n et ajouter l\'attribut @corresp="NUM" avec NUM le numéro de la prise de parole correspondante en colonne de gauche\nPar exemple :\n</xsl:text>
                                                  <xsl:text>  &lt;sp  corresp="428"&gt;\n    &lt;speaker corresp="ΧΟΡΟΣ ">LE CORYPHÉE&lt;/speaker>\n</xsl:text>
                                                  <xsl:text>    &lt;p>&lt;stage>S\'interposant pour les empêcher d\'en venir aux coups.&lt;/stage> Assez de (...)\n    &lt;/p>\n  &lt;/sp></xsl:text>
                                                  <xsl:text>\n !!! Indiquer "none" si pas de correspondance dans la source.</xsl:text>
                                                  <xsl:text>NB technique : speaker ref=</xsl:text>
                                                  <xsl:value-of
                                                  select="string-to-codepoints($spOne)"/>
                                                  <xsl:text> ; speaker edition = </xsl:text>
                                                  <xsl:value-of
                                                  select="string-to-codepoints($spTwo)"/>
                                                  <xsl:text>')\n</xsl:text>
                                                  </xsl:attribute>
                                                  <i
                                                  class="fa fa-exclamation-circle fa-3 float-start mr-1"
                                                  aria-hidden="true"
                                                  title="Cliquer pour avoir plus d'information"/>
                                                  <small class="font-weight-light">correspondance à
                                                  corriger</small>
                                                  </span>
                                                  </xsl:if>
                                                  <xsl:apply-templates
                                                  select="$current_edition//tei:sp[count(. | preceding::tei:sp) = $correspTwo]"
                                                  />
                                                  </td>
                                                  </tr>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                            <xsl:if test="count(./following-sibling::tei:sp) = 0">
                                                <tr>
                                                  <td/>
                                                  <td/>
                                                  <td/>
                                                  <td class="text-center">
                                                  <hr/>
                                                  </td>
                                                </tr>
                                            </xsl:if>
                                        </xsl:for-each>
                                        <xsl:if
                                            test="count($reference_edition/tei:TEI//tei:sp) > count($current_edition//tei:sp)">
                                            <xsl:if test="$DEBUG">
                                                <xsl:message>
                                                  <xsl:text disable-output-escaping="yes">#sp L > #sp R so finish L column</xsl:text>
                                                </xsl:message>
                                            </xsl:if>
                                            <xsl:for-each
                                                select="$reference_edition/tei:TEI//tei:sp[count(. | preceding::tei:sp) > count($reference_edition/tei:TEI//tei:sp)]">
                                                <xsl:message>
                                                  <xsl:text>  </xsl:text>
                                                  <xsl:value-of
                                                  select="count(. | preceding::tei:sp) + count($reference_edition/tei:TEI//tei:sp)"/>
                                                  <xsl:text> / -</xsl:text>
                                                </xsl:message>
                                                <tr class="tr-corresp">
                                                  <td class="correspOne">
                                                  <xsl:value-of
                                                  select="count(. | preceding::tei:sp) + count($reference_edition/tei:TEI//tei:sp)"
                                                  />
                                                  </td>
                                                  <td class="one">
                                                  <xsl:apply-templates select="."/>
                                                  </td>
                                                  <td class="correspTwo">-</td>
                                                  <td class="two aligned">-</td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:if>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="p-3">
                    <h2>Notes</h2>
                    <ul style="list-style-type: none;">
                        <xsl:apply-templates select="//tei:note" mode="do"/>
                    </ul>
                </div>
                <a id="back-to-top" href="#"
                    class="btn btn-secondary btn-sm btn-circle navigation-btn" role="button"
                    title="Back to Top" data-toggle="tooltip" data-placement="top">
                    <i class="fa fa-chevron-up" data-toggle="tooltip"/>
                </a>
                <a id="previous-warning-group" class="btn btn-warning btn-sm navigation-btn"
                    role="button" title="Aller au précédant groupe d'alignement à corriger"
                    data-toggle="tooltip" data-placement="top" onclick="previous_tr_warning_group()">
                    <i class="fa fa-angle-double-up" data-toggle="tooltip"/>
                </a>
                <a id="previous-warning" class="btn btn-warning btn-sm navigation-btn" role="button"
                    title="Aller au précédant alignement à corriger" data-toggle="tooltip"
                    data-placement="top" onclick="previous_tr_warning()">
                    <i class="fa fa-angle-up" data-toggle="tooltip"/>
                </a>
                <a id="next-warning" class="btn btn-warning btn-sm navigation-btn" role="button"
                    title="Aller au prochain alignement à corriger" data-toggle="tooltip"
                    data-placement="top" onclick="next_tr_warning()">
                    <i class="fa fa-angle-down" data-toggle="tooltip"/>
                </a>
                <a id="next-warning-group" class="btn btn-warning btn-sm navigation-btn"
                    role="button" title="Aller au prochain groupe d'alignement à corriger"
                    data-toggle="tooltip" data-placement="top" onclick="next_tr_warning_group()">
                    <i class="fa fa-angle-double-down" data-toggle="tooltip"/>
                </a>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="getCorrespOne">
        <xsl:param name="correspTwo"/>
        <xsl:choose>
            <xsl:when
                test="exists($current_edition//tei:sp[count(. | preceding::tei:sp) = $correspTwo]/@corresp)">
                <xsl:if test="$DEBUG">
                    <xsl:message> @corresp declared. getCorrespOne(<xsl:value-of
                            select="$correspTwo"/>) returned: <xsl:value-of
                            select="$current_edition//tei:sp[count(. | preceding::tei:sp) = $correspTwo]/@corresp"
                        /></xsl:message>
                </xsl:if>
                <xsl:value-of
                    select="$current_edition//tei:sp[count(. | preceding::tei:sp) = $correspTwo]/@corresp"
                />
            </xsl:when>
            <xsl:when test="$correspTwo = 1">
                <xsl:if test="$DEBUG">
                    <xsl:message> first R and no @corresp declared. getCorrespOne(<xsl:value-of
                            select="$correspTwo"/>) returned: 1</xsl:message>
                </xsl:if>
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when
                        test="count($current_edition//tei:sp[@corresp and not(@corresp = 'none') and not(empty(@corresp)) and xs:integer($correspTwo) > count(. | preceding::tei:sp)]) > 0">
                        <xsl:if test="$DEBUG">
                            <xsl:message> There is a previous @corresp declared!</xsl:message>
                        </xsl:if>

                        <xsl:variable name="lastCorresp">
                            <!-- select="$current_edition//tei:sp[not(@corresp = 'none') and $correspTwo > @corresp][last()]/@corresp" -->
                            <xsl:value-of
                                select="($current_edition//tei:sp[not((@corresp = 'none') or not(@corresp)) and $correspTwo > count(. | preceding::tei:sp)][last()]/@corresp)[last()]"
                            />
                        </xsl:variable>

                        <xsl:variable name="lastPos">
                            <!-- R lastPos printed -->
                            <!-- select="count($current_edition//tei:sp[not(@corresp = 'none') and $correspTwo > @corresp][last()]/preceding-sibling::tei:sp) + 1"/> -->
                            <!--<xsl:value-of
                                select="count(($current_edition//tei:sp[not((@corresp = 'none') or not(@corresp)) and $correspTwo > count(. | preceding::tei:sp)])[last()]/preceding-sibling::tei:sp)
                                        + 1"
                            />-->
                            <xsl:value-of
                                select="($current_edition//tei:sp[not((@corresp = 'none') or not(@corresp)) and $correspTwo > count(. | preceding::tei:sp)][last()])[last()]/count(. | preceding::tei:sp)"
                            />
                        </xsl:variable>
                        <xsl:if test="$DEBUG">
                            <xsl:message>
                                <xsl:text> L : "</xsl:text>
                                <xsl:value-of select="$correspTwo"/>
                                <xsl:text>" - Last @corresp : "</xsl:text>
                                <xsl:value-of select="$lastCorresp"/>
                                <xsl:text>" for R (lastPos) "</xsl:text>
                                <xsl:value-of select="$lastPos"/>
                                <xsl:text>". So current shloud be: </xsl:text>
                                <xsl:value-of select="xs:integer(tokenize(xs:string($correspTwo), ' ')[1]) - ($lastPos - xs:integer(tokenize(xs:string($lastCorresp), ' ')[1]))"/>
                            </xsl:message>
                        </xsl:if>
                        <xsl:if test="$DEBUG">
                            <xsl:message> getCorrespOne(<xsl:value-of select="$correspTwo"/>)
                                returned: <xsl:value-of
                                    select="xs:integer(tokenize(xs:string($correspTwo), ' ')[1]) - ($lastPos - xs:integer(tokenize($lastCorresp, ' ')[1]))"/></xsl:message>
                        </xsl:if>
                        <xsl:value-of select="xs:integer(tokenize(xs:string($correspTwo), ' ')[1]) - ($lastPos - xs:integer(tokenize($lastCorresp, ' ')[1]))"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:if test="$DEBUG">
                            <xsl:message> no @corresp declared previously.
                                    getCorrespOne(<xsl:value-of select="$correspTwo"/>) returned:
                                    <xsl:value-of select="$correspTwo"/></xsl:message>
                        </xsl:if>
                        <xsl:value-of select="$correspTwo"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="getPreviousCorrespOne">
        <xsl:param name="correspTwo"/>
        <xsl:variable name="previousCorrespOne">
            <xsl:if test="$DEBUG">
                <xsl:message> getCorrespOne(<xsl:value-of select="xs:integer($correspTwo - 1)"
                    />)</xsl:message>
            </xsl:if>
            <xsl:call-template name="getCorrespOne">
                <xsl:with-param name="correspTwo" select="xs:integer($correspTwo - 1)"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$previousCorrespOne = 'none'">
                <xsl:if test="$DEBUG">
                    <xsl:message> getPreviousCorrespOne(<xsl:value-of
                            select="xs:integer($correspTwo - 1)"/>)</xsl:message>
                </xsl:if>
                <xsl:call-template name="getPreviousCorrespOne">
                    <xsl:with-param name="correspTwo" select="xs:integer($correspTwo - 1)"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="$DEBUG">
                    <xsl:message> getPreviousCorrespOne return: <xsl:value-of
                            select="$previousCorrespOne"/></xsl:message>
                </xsl:if>
                <xsl:value-of select="$previousCorrespOne"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:head">
        <i class="fa fa-file-text"/>
        <xsl:text> </xsl:text>
        <b>
            <xsl:apply-templates/>
        </b>

    </xsl:template>

    <xsl:template match="tei:speaker">
        <span class="speaker" title="{@corresp}">
            <xsl:if test="parent::tei:sp[@corresp]">
                <span class="float-start small text-secondary" title="Correspondance déclarée"
                        >(<xsl:value-of select="parent::tei:sp/@corresp"/>
                    <i class="fa fa-arrows-h small"/>)</span>
                <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:p">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="tei:title[@type = 'main']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>

    <xsl:template match="tei:editor">
        <xsl:apply-templates/>
        <xsl:if test="@role">
            <xsl:text> (</xsl:text>
            <xsl:value-of select="@role"/>
            <xsl:text>)</xsl:text>
        </xsl:if>
        <xsl:if test="not(substring(text(), string-length(text()), 1) = '.')">.</xsl:if>
    </xsl:template>

    <xsl:template match="tei:date">
        <xsl:apply-templates/>
        <xsl:if test="not(substring(text(), string-length(text()), 1) = '.')">.</xsl:if>
    </xsl:template>

    <xsl:template match="tei:publisher">
        <xsl:apply-templates/>
        <xsl:if test="not(substring(text(), string-length(text()), 1) = '.')">.</xsl:if>
    </xsl:template>

    <xsl:template match="tei:pubPlace">
        <xsl:apply-templates/>
        <xsl:if test="not(substring(text(), string-length(text()), 1) = '.')">.</xsl:if>
    </xsl:template>

    <xsl:template match="tei:biblScope">
        <xsl:value-of select="concat(upper-case(substring(@unit, 1, 1)), substring(@unit, 2))"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="."/>
        <xsl:if test="not(substring(text(), string-length(text()), 1) = '.')">.</xsl:if>
    </xsl:template>

    <xsl:template match="tei:stage">
        <p>
            <i>
                <xsl:apply-templates/>
            </i>
        </p>
    </xsl:template>

    <xsl:template match="tei:pb">
        <i class="fa fa-file-text-o float-start text-secondary small mr-1" aria-hidden="true"
            title="Nouvelle page. Balise TEI &lt;pb/&gt;."/>
    </xsl:template>

    <xsl:template match="tei:fw">
        <i class="fa float-start text-secondary small ml-1"
            title="Numéro de page ou autre élément d'entête. Balise TEI &lt;fw&gt;.">
            <xsl:apply-templates/>
        </i>
    </xsl:template>

    <xsl:template match="tei:sp">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="tei:sourceDesc"/>

    <xsl:template match="tei:l">
        <xsl:apply-templates/>
        <br/>
    </xsl:template>

    <xsl:template match="tei:num[@type = 'verse']">
        <span class="verse-number" title="Numéro de vers">
            <xsl:apply-templates/>
        </span>
        <br/>
    </xsl:template>

    <xsl:template match="tei:foreign">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>

    <xsl:template match="tei:ref">
        <sup>
            <xsl:attribute name="id">
                <xsl:text>call_</xsl:text>
                <xsl:value-of select="substring-after(@target, '#')"/>
            </xsl:attribute>
            <a href="{@target}">
                <xsl:apply-templates/>
            </a>
        </sup>
    </xsl:template>

    <xsl:template match="tei:note">
        <xsl:if test="not(exists(//tei:ref[@target = ./@xml:id]))">
            <xsl:variable name="note_id">
                <xsl:number level="any"/>
            </xsl:variable>
            <sup>
                <xsl:attribute name="id">
                    <xsl:text>call_n_</xsl:text>
                    <xsl:value-of select="$note_id"/>
                </xsl:attribute>
                <xsl:if test="@place = 'inline'">
                    <xsl:attribute name="style">
                        <xsl:text>top: revert; font-size: revert; line-height: revert;</xsl:text>
                    </xsl:attribute>
                </xsl:if>
                <a>
                    <xsl:attribute name="href">
                        <xsl:text>#n_</xsl:text>
                        <xsl:value-of select="$note_id"/>
                    </xsl:attribute>
                    <xsl:value-of select="$note_id"/>
                </a>
            </sup>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:note" mode="do">
        <xsl:variable name="note_id">
            <xsl:choose>
                <xsl:when test="not(@xml:id)">
                    <xsl:text>n_</xsl:text>
                    <xsl:number level="any"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="@xml:id"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <li id="{$note_id}">
            <a>
                <xsl:attribute name="href">
                    <xsl:text>#call_</xsl:text>
                    <xsl:value-of select="$note_id"/>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="@n"><xsl:value-of select="@n"/>. </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="substring-after($note_id, 'n_')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </a>
            <xsl:text> </xsl:text>
            <xsl:apply-templates/>
            <xsl:text> </xsl:text>
            <xsl:if test="@resp">[<xsl:value-of select="substring-after(@resp, '#')"/>]</xsl:if>
            <xsl:text> </xsl:text>
            <a>
                <xsl:attribute name="href">
                    <xsl:text>#call_</xsl:text>
                    <xsl:value-of select="$note_id"/>
                </xsl:attribute>
                <i class="fa fa-arrow-up" style="cursor: auto;"/>
            </a>
        </li>
    </xsl:template>

    <xsl:template match="tei:*">
        <abbr class="text-warning"
            title="Balise &lt;{name(.)}&gt; non gérée. Le contenu s'affiche tel quel.">
            <i class="fa fa-info fa-3 float-start mr-1"/>
        </abbr>
        <xsl:apply-templates/>
    </xsl:template>

</xsl:stylesheet>
